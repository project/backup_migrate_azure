<?php

namespace Drupal\backup_migrate_azure\Plugin\BackupMigrateDestination;

use Drupal\backup_migrate\Drupal\EntityPlugins\DestinationPluginBase;

/**
 * Defines a file directory destination plugin.
 *
 * @BackupMigrateDestinationPlugin(
 *   id = "azure",
 *   title = @Translation("Azure"),
 *   description = @Translation("Backup to Azure Blob storage."),
 *   wrapped_class = "\Drupal\backup_migrate_azure\Destination\AzureDestination"
 * )
 *
 * Note: It is unclear why the plugin is constructed with wrapped_class. To keep
 * things consistent, the deprecated Psr4ClassLoader is called in
 * backup_migrate_azure.module.
 *
 * @see \Drupal\backup_migrate\Drupal\EntityPlugins\WrapperPluginBase::getObject()
 */
class AzureDestinationPlugin extends DestinationPluginBase {
}
