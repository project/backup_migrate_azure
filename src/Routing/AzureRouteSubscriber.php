<?php

namespace Drupal\backup_migrate_azure\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Azure Backup & Migrate Route Subscriber.
 */
class AzureRouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('entity.backup_migrate_destination.backup_download')) {
      $route->setDefaults([
        '_controller' => '\Drupal\backup_migrate_azure\Controller\AzureBackupController::download',
      ]);
    }
  }

}
