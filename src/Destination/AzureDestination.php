<?php

namespace Drupal\backup_migrate_azure\Destination;

use MicrosoftAzure\Storage\Blob\BlobRestProxy;
use MicrosoftAzure\Storage\Blob\Models\ListBlobsOptions;
use MicrosoftAzure\Storage\Common\Exceptions\ServiceException;
use MicrosoftAzure\Storage\Blob\Models\PutBlobResult;

use Drupal\backup_migrate\Core\Config\ConfigInterface;
use Drupal\backup_migrate\Core\Config\ConfigurableInterface;
use Drupal\backup_migrate\Core\Exception\BackupMigrateException;
use Drupal\backup_migrate\Core\Destination\RemoteDestinationInterface;
use Drupal\backup_migrate\Core\Destination\ListableDestinationInterface;
use Drupal\backup_migrate\Core\File\BackupFile;
use Drupal\backup_migrate\Core\Destination\ReadableDestinationInterface;
use Drupal\backup_migrate\Core\File\BackupFileInterface;
use Drupal\backup_migrate\Core\File\BackupFileReadableInterface;
use Drupal\backup_migrate\Core\Destination\DestinationBase;

use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Url;

use Symfony\Component\HttpFoundation\Response;

/**
 * Azure Backup & Migrate Destination.
 *
 * @package Drupal\backup_migrate_aws_s3\Destination
 */
class AzureDestination extends DestinationBase implements RemoteDestinationInterface, ListableDestinationInterface, ReadableDestinationInterface, ConfigurableInterface {

  use MessengerTrait;
  use LoggerChannelTrait;

  /**
   * Microsoft Blob client.
   *
   * @var \MicrosoftAzure\Storage\Blob\BlobRestProxy
   */
  protected $client = NULL;

  /**
   * Key repository service.
   *
   * @var \Drupal\key\KeyRepository
   */
  protected $keyRepository = NULL;

  /**
   * File repository service.
   *
   * @var \Drupal\file\FileRepository
   */
  protected $fileRepository = NULL;

  /**
   * Filesystem service.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem = NULL;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigInterface $config) {
    parent::__construct($config);

    /** @codingStandardsIgnoreStart */

    /**
     * @var \Drupal\key\KeyRepository keyRepository
     */
    $this->keyRepository = \Drupal::service('key.repository');

    /**
     * @var \Drupal\file\FileRepository fileRepository
     */
    $this->fileRepository = \Drupal::service('file.repository');

    /**
     * @var \Drupal\Core\File\FileSystem fileSystem
     */
    $this->fileSystem = \Drupal::service('file_system');

    /** @codingStandardsIgnoreEnd */
  }

  /**
   * Download backup file.
   *
   * @param \Drupal\backup_migrate\Core\File\BackupFileInterface $file
   *   Backup file.
   *
   * @return \Symfony\Component\HttpFoundation\Response|void
   *   Returns http file response.
   */
  public function downloadFile(BackupFileInterface $file) {
    $id = $file->getMeta('id');
    if ($this->fileExists($id)) {
      try {
        $blobResult = $this->getClient()
          ->getBlob($this->getContainerName(), $file->getFullName());
        if ($blobResult) {
          return new Response(
            stream_get_contents($blobResult->getContentStream()),
            Response::HTTP_OK,
            ['content-type' => $blobResult->getProperties()->getContentType()]
          );
        }
        else {
          $this->getLogger('backup_migrate_azure')
            ->warning('Blob result was not found. Unable to download backup file.');
        }
      }
      catch (ServiceException $e) {
        watchdog_exception('backup_migrate_azure - downloadFile()', $e);
      }
    }
    else {
      $this->getLogger('backup_migrate_azure')
        ->warning('Azure backup file was not found. Unable to download backup file.');
    }
  }

  /**
   * Get Azure container name.
   *
   * @return mixed
   *   Returns container name.
   */
  private function getContainerName() {
    return $this->confGet('azure_container_name');
  }

  /**
   * Get Azure account key.
   *
   * @return mixed|string|null
   *   Returns account key.
   */
  private function getAccessKey() {
    $accountKey = NULL;
    $accountKeyName = $this->confGet('azure_access_key_name');
    if (!empty($accountKeyName)) {
      $accountKey = $this->keyRepository->getKey($accountKeyName)
        ->getKeyValue();
    }
    return $accountKey;
  }

  /**
   * Get Azure client.
   *
   * @return \MicrosoftAzure\Storage\Blob\BlobRestProxy|null
   *   Returns client.
   */
  public function getClient(): ?BlobRestProxy {
    // Check to see if client is already set.
    if ($this->client == NULL) {

      // Get access key.
      $accessKey = $this->getAccessKey();
      if (empty($accessKey)) {
        $this->getLogger('backup_migrate_azure')
          ->warning('Azure access key not configured.');
        return NULL;
      }

      // Get account name.
      $accountName = $this->confGet('azure_account_name');
      if (empty($accountName)) {
        $this->getLogger('backup_migrate_azure')
          ->warning('Azure account name not configured.');
        return NULL;
      }

      // Build connection string.
      $connectionString = "DefaultEndpointsProtocol=https;AccountName={$accountName};AccountKey={$accessKey}";

      // Set client.
      $this->client = BlobRestProxy::createBlobService($connectionString);
    }

    // Return client.
    return $this->client;
  }

  /**
   * {@inheritdoc}
   */
  public function checkWritable(): bool {
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  protected function deleteTheFile($id) {
    if ($this->fileExists($id)) {
      try {
        $this->getClient()->deleteBlob($this->getContainerName(), $id);
      }
      catch (ServiceException $e) {
        watchdog_exception('backup_migrate_azure - deleteTheFile()', $e);
      }
    }
    else {
      $this->getLogger('backup_migrate_azure')->warning('Unable to delete backup file. File was not found.');
    }
  }

  /**
   * {@inheritDoc}
   *
   * @throws \Drupal\backup_migrate\Core\Exception\BackupMigrateException
   */
  protected function saveTheFile(BackupFileReadableInterface $file): ?PutBlobResult {
    $result = NULL;
    if (file_exists($file->realpath())) {
      try {
        $result = $this->getClient()
          ->createBlockBlob($this->getContainerName(), $file->getFullName(), $file->openForRead());
      }
      catch (ServiceException $e) {
        $this->getLogger('backup_migrate_azure')->error($e->getMessage());
        throw new BackupMigrateException('Could not upload to Azure: %err (code: %code)', [
          '%err' => $e->getMessage(),
          '%code' => $e->getCode(),
        ]);
      }
    }
    else {
      $this->getLogger('backup_migrate_azure')->warning('Backup file was not found.');
    }
    return $result;
  }

  /**
   * {@inheritDoc}
   */
  protected function saveTheFileMetadata(BackupFileInterface $file) {
    // Nothing to do here.
  }

  /**
   * {@inheritDoc}
   */
  protected function loadFileMetadataArray(BackupFileInterface $file) {
    // Nothing to do here.
  }

  /**
   * {@inheritDoc}
   */
  public function listFiles() {
    $files = [];

    try {
      // List blobs.
      $listBlobsOptions = new ListBlobsOptions();

      // Set prefix.
      $prefix = $this->confGet('azure_prefix');
      if (!empty($prefix)) {
        $listBlobsOptions->setPrefix($prefix);
      }

      // Setting max result to 1 is just to demonstrate the continuation token.
      // It is not the recommended value in a product environment.
      $listBlobsOptions->setMaxResults(1);

      do {
        $blob_list = $this->getClient()
          ->listBlobs($this->getContainerName(), $listBlobsOptions);
        foreach ($blob_list->getBlobs() as $blob) {
          // Setup new backup file.
          $backupFile = new BackupFile();
          $backupFile->setMeta('id', $blob->getName());
          $backupFile->setMeta('filesize', $blob->getProperties()
            ->getContentLength());
          $backupFile->setMeta('datestamp', $blob->getProperties()
            ->getCreationTime()
            ->getTimestamp());
          $backupFile->setFullName($blob->getName());
          $backupFile->setMeta('metadata_loaded', TRUE);

          // Add backup file to array.
          $files[$blob->getName()] = $backupFile;
        }

        $listBlobsOptions->setContinuationToken($blob_list->getContinuationToken());
      } while ($blob_list->getContinuationToken());

    }
    catch (ServiceException $e) {
      watchdog_exception('backup_migrate_azure - listFiles()', $e);
    }

    return $files;
  }

  /**
   * {@inheritDoc}
   */
  public function queryFiles(array $filters = [], $sort = 'datestamp', $sort_direction = SORT_DESC, $count = 100, $start = 0) {
    // Get the full list of files.
    $out = $this->listFiles($count + $start);
    foreach ($out as $key => $file) {
      $out[$key] = $this->loadFileMetadata($file);
    }

    // Filter the output.
    $out = array_reverse($out);

    // Slice the return array.
    if ($count || $start) {
      $out = array_slice($out, $start, $count);
    }

    return $out;
  }

  /**
   * {@inheritDoc}
   */
  public function countFiles() {
    $file_list = $this->listFiles();
    return count($file_list);
  }

  /**
   * {@inheritDoc}
   */
  public function getFile($id) {
    $files = $this->listFiles();
    if (isset($files[$id])) {
      return $files[$id];
    }
    return NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function loadFileForReading(BackupFileInterface $file) {
    // If this file is already readable, simply return it.
    if ($file instanceof BackupFileReadableInterface) {
      return $file;
    }
    $id = $file->getMeta('id');
    try {
      $blobResult = $this->getClient()->getBlob($this->getContainerName(), $id);
      return $blobResult->getContentStream();
    }
    catch (ServiceException $e) {
      watchdog_exception('backup_migrate_azure - loadFileForReading()', $e);
    }
    return NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function fileExists($id): bool {
    return (boolean) $this->getFile($id);
  }

  /**
   * Init configurations.
   *
   * @param array $params
   *   Array of params.
   *
   * @return array
   *   Returns config schema.
   */
  public function configSchema(array $params = []): array {
    $schema = [];

    // Init settings.
    if ($params['operation'] == 'initialize') {

      $key_collection_url = Url::fromRoute('entity.key.collection')->toString();

      // Get available keys.
      $keys = $this->keyRepository->getKeys();
      $key_options = [];
      foreach ($keys as $key_id => $key) {
        $key_options[$key_id] = $key->label();
      }

      // Access key.
      $schema['fields']['azure_access_key_name'] = [
        'type' => 'enum',
        'title' => $this->t('Azure Access Key'),
        'description' => $this->t('Access key to use Azure client. Use keys managed by the key module. <a href=":keys">Manage keys</a>', [
          ':keys' => $key_collection_url,
        ]),
        'empty_option' => $this->t('- Select Key -'),
        'options' => $key_options,
        'required' => TRUE,
      ];

      // Account name.
      $schema['fields']['azure_account_name'] = [
        'type' => 'text',
        'title' => $this->t('Azure Account Name'),
        'required' => TRUE,
        'description' => $this->t('Name of account used in Azure storage.'),
      ];

      // Container name.
      $schema['fields']['azure_container_name'] = [
        'type' => 'text',
        'title' => $this->t('Azure Container Name'),
        'required' => TRUE,
        'description' => $this->t('Name of container used in Azure storage.'),
      ];

      // Folder prefix.
      $schema['fields']['azure_prefix'] = [
        'type' => 'text',
        'title' => $this->t('Sub-folder within the Azure (optional)'),
        'required' => FALSE,
        'description' => $this->t('If you wish to organise your backups into a sub-folder such as /my/subfolder/, enter <i>my/subfolder</i> here without the leading or trailing slashes.'),
      ];
    }

    return $schema;
  }

}
